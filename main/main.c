/*
 * main.c
 *
 *  Created on: 20.09.2017
 *      Author: annetta897
 */
#include <avr/io.h>
#include <usart.h>
#include <timers.h>
#include <nrf.h>
#include <util/delay.h>

int main (void)
{
	char data_w[PAYLOAD_LENGTH] = "Hello, world!\n";
	char data_r[PAYLOAD_LENGTH];
	USARTInit ();
	TIM1Init();
	_delay_ms (200);
	while (1){
		RadioConfig(transmitt);
		RadioSendCmd(FLUSH_TX, 0, 0, 0);
		RadioSetRegVal(STATUS, 0x7F);
		RadioWriteData(data_w);
		while (!(RadioGetStatus() & ((1<<4)|(1<<5))))
			;
		RadioConfig(receive);
		TIM1SetVal(0);
		while(TIM1GetVal() < 1000000ul){
			if(RadioDataReceived()){
				RadioReadData (data_r);
				USARTSend(data_r, PAYLOAD_LENGTH);
				RadioSetRegVal(STATUS, 0x7F);
			}
		}
	}
	return 0;
}

